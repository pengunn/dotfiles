# ~/.zshrc

# Options
setopt correct
setopt autocd
setopt nobeep

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"


# History settings
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Load
autoload -Uz compinit && compinit
autoload -Uz promptinit && promptinit
autoload -Uz vcs_info
autoload -Uz run-help

bindkey -v      # vim style

# Source config files
for f in $HOME/.config/zsh/*.zsh; do
    source $f
done
