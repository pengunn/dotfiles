# ~/etc/zsh/prompt.zsh

# Colors
local ACCENT=cyan

# Section variables
local PTIME="%F{$ACCENT}[%f%t %F{$ACCENT}]%f"
local PPWD="%F{$ACCENT}[%f %~ %F{$ACCENT}]%f"

# Set the prompt
PROMPT="$PTIME$PPWD
%F{cyan}>> %f"
