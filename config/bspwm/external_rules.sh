#!/usr/bin/env sh

wid=$1
class=$2
instance=$3

# Get info of focused window
wininfo=$(xwininfo -id $(bspc query -N -n focused))
fwidth=$(echo "$wininfo" | grep Width | awk '{print $2}')
fheight=$(echo "$wininfo" | grep Height | awk '{print $2}')

# Exit if there is a preselection so we don't mess with it
pre="$(bspc query -N -d -n .\!automatic)"
[ ! -z $pre ] && exit 0

# Split based on window size
if [ $fwidth -gt $fheight ]; then
    echo "split_dir=east"
else
    echo "split_dir=south"
fi
