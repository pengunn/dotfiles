# ~/.config/zsh/functions.zsh

# Get battery level
bat() {
    local BATLEVEL=`acpi | awk '{print $4}' | tr -d ","`
    echo $BATLEVEL
}

setwall() {
    if [ -f $1 ]; then
        local LINK="$HOME/.config/wallpaper.jpg"
        local WALLPATH="$(realpath $1)"
        
        ln -sf $WALLPATH $LINK
    else
        echo "Could not find image $1!"
    fi
}

colorscheme() {
    if [ -f $COLORFILE ]; then
        local COLORDIR="$HOME/.config/colors"
        local COLORFILE="$COLORDIR/$1"
        local CURRFILE="$COLORDIR/current"
        
        ln -sfr $COLORFILE $CURRFILE
    else
       echo "Could not find color theme $1 in $COLORDIR!"
   fi
}
