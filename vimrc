" ~/.vimrc

" Ensures that various options are properly set to work with the Vim-related
" packages available in Debian
runtime! debian.vim

set nocompatible              " be iMproved

" Vundle stuff ---------------------------------------------------------------
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
Plugin 'VundleVim/Vundle.vim'

" Plugins

Plugin 'dracula/vim',{'name':'dracula'}       " sickest color scheme
Plugin 'scrooloose/nerdtree'                " file tree
Plugin 'tpope/vim-fugitive'                 " git integration
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-tbone'
Plugin 'tpope/vim-commentary'               " comment line with gcc or gc for target of a motion
" Plugin 'valloric/youcompleteme'
Plugin 'raimondi/delimitmate'               " auto-close brackets, quotes, parens, etc.
Plugin 'itchyny/lightline.vim'              " lightweight status bar
" Plugin 'ap/vim-buftabline'                  " adds buffer tab line
Plugin 'christoomey/vim-tmux-navigator'     " navigate seamlessly between vim and tmux
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'

" End Plugins

call vundle#end()
filetype plugin indent on
" End Vundle stuff


" Settings -------------------------------------------------------------------
" General
set title		        " sets title of term to file name
set mouse=a		      " enable mouse use

set modeline		    " use modelines
set modelines=5

set hidden          " allows buffers to be open even when not visible

set noshowmode		  " hide mode in bottome left since it's in airline

" Syntax rules
au BufNewFile,BufRead /*.rasi setf css

" Editor
if has("syntax")
    syntax on
endif

set t_Co=256        " enable 256 color mode
set termguicolors
"set background=dark
colorscheme dracula
let &t_ut=''        " Some magic that lets the backround not mess ip in kitty

set relativenumber  " relative line numbering
set number		      " line numbering
set cursorline		  " highight cursor line
set showmatch		    " show matching brackets
set background=dark	" use dark background
"set colorcolumn=80  " color the 80th column

set tabstop=2		    " tabs appear as 4 spaces
set shiftwidth=2	  " when indenting, use 4 spaces width
set expandtab		    " use spaces when tab key is pressed
set smarttab        " curser moves to next tabstop when at beginning of line

set whichwrap+=<,>,h,l,[,]	" causes pressing left/right to wrap to next/prev line

set splitright		  " open new vertically-split pane to the right
set splitbelow		  " open new horizontally-split pane below

" Matching
set ignorecase		  " case insensitve matching
set smartcase		    " case sensitive when capital is used

" Wrap git commits at 72nd column and turn on spell checker
autocmd Filetype gitcommit setlocal spell textwidth=72

" Plugins
set laststatus=2    " show statusline without split

" Lightline settings
" Use these if you want powerline patched font separators:
"   separator:    left: \ue0b0, right: \ue0b2
"   subseparator: left: \ue0b1, right: \ue0b3

let lllsep = "\ue0b0"
let llrsep = "\ue0b2"
let lllssep = "\ue0b1"
let llrssep = "\ue0b3"

let g:lightline = {
    \ 'colorscheme': 'nord',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'fugitive', 'filename' ] ]
    \ },
    \ 'component_function': {
    \   'fugitive': 'LightLineFugitive',
    \   'readonly': 'LightLineReadonly',
    \   'modified': 'LightLineModified',
    \   'filename': 'LightLineFilename',
    \   'fileformat': 'LightlineFileformat',
    \   'filetype': 'LightlineFiletype',
    \   'fileencoding': 'LightlineFileencoding'
    \ },
    \ 'separator': { 'left': lllsep, 'right': llrsep },
    \ 'subseparator': { 'left': lllssep, 'right': llrssep }
    \ }

function! LightLineModified()
    if &filetype == "help"
        return ""
    elseif &modified
        return "+"
    elseif &modifiable
        return ""
    else
        return ""
    endif
endfunction

function! LightLineReadonly()
    if &filetype == "help"
        return ""
    elseif &readonly
        return "\ue0a2"
    else
        return ""
    endif
endfunction

function! LightLineFugitive()
    if exists("*fugitive#head")
        let _ = fugitive#head()
        return strlen(_) ? "\ue0a0 "._ : ''
    endif
    return ''
endfunction

function! LightLineFilename()
    return ('' != LightLineReadonly() ? LightLineReadonly() . ' ' : '') .
        \ ('' != expand('%:t') ? expand('%:t') : '[No Name]') .
        \ ('' != LightLineModified() ? ' ' . LightLineModified() : '')
endfunction

function! LightlineFileformat()
    return winwidth(0) > 70 ? &fileformat : ''
endfunction

function! LightlineFiletype()
    return winwidth(0) > 70 ? (&filetype !=# '' ? &filetype : 'no ft') : ''
endfunction

function! LightlineFileencoding()
    return winwidth(0) > 70 ? (&fenc !=# '' ? &fenc : &enc) : ''
endfunction

let delimitMate_expand_cr=1     " allow delmitmate to expand brackets and put cursur tabbed between

"autocmd vimenter * NERDTree     " start nerdtree when vim starts and close when last window
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Keybindings -------------------------------------------------------
nnoremap <C-J> <C-W><C-J>	" ctrl-j to go to window below
nnoremap <C-K> <C-W><C-K>	" ctrl-k to go to window above
nnoremap <C-L> <C-W><C-L>	" ctrl-l to go to window right
nnoremap <C-H> <C-W><C-H>	" ctrl-h to go to window left

nnoremap <C-P> :tabp<CR>
nnoremap <C-N> :tabn<CR>

nnoremap <C-X> :NERDTreeToggle<CR>
