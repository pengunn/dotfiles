#!/bin/zsh
#
# Sources colors from ~/.Xresources and exports the mas environment vars
#

# File to get the colors from
CF="$HOME/.config/colors/current"

# background
export THEMEBG=$(cat ${CF} | grep -i THEMEBG | tail -c 8)
# forground
export THEMEFG=$(cat ${CF} | grep -i THEMEFG | tail -c 8)

# Black
export THEMEBLK=$(cat ${CF} | grep -i THEMEBLK | tail -c 8)
export THEMEBLKB=$(cat ${CF} | grep -i THEMEBLKB | tail -c 8)

# Red
export THEMERED=$(cat ${CF} | grep -i THEMERED | tail -c 8)
export THEMEREDB=$(cat ${CF} | grep -i THEMEREDB | tail -c 8)

# Green
export THEMEGRN=$(cat ${CF} | grep -i THEMEGRN | tail -c 8)
export THEMEGRNB=$(cat ${CF} | grep -i THEMEGRNB | tail -c 8)

# Yellow
export THEMEYEL=$(cat ${CF} | grep -i THEMEYEL | tail -c 8)
export THEMEYELB=$(cat ${CF} | grep -i THEMEYELB | tail -c 8)

# Blue
export THEMEBLU=$(cat ${CF} | grep -i THEMEBLU | tail -c 8)
export THEMEBLUB=$(cat ${CF} | grep -i THEMEBLUB | tail -c 8)

# Magenta
export THEMEMAG=$(cat ${CF} | grep -i THEMEMAG | tail -c 8)
export THEMEMAGB=$(cat ${CF} | grep -i THEMEMAGB | tail -c 8)

# Cyan
export THEMECYA=$(cat ${CF} | grep -i THEMECYA | tail -c 8)
export THEMECYAB=$(cat ${CF} | grep -i THEMECYAB | tail -c 8)

# White
export THEMEWHT=$(cat ${CF} | grep -i THEMEWHT | tail -c 8)
export THEMEWHTB=$(cat ${CF} | grep -i THEMEWHTB | tail -c 8)
