# Dotfiles
Repo for my configuration files and setup scripts

### Instructions
* Clone this repo to ~/.dotfiles
* Run scripts in the install directory to get everything setup
  * Change BASEDIR in install/dots if cloning to somewhere besides ~./dotfiles
