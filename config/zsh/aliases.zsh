#
# ~/.bash_aliases
#

# ls
alias ls='ls --color=auto --group-directories-first'
alias la='ls -a'
alias ll='ls -AlF'

# grep colors
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# python
alias aenv='source env/bin/activate'
alias manage='python manage.py'

# tmux
alias tmux='tmux -2'
alias tnew='tmux new-session -s'
alias tatach='tmux attach-session -t'
alias tlist='tmux list-sessions'

# config editing
if [ -n "${EDITOR+1}" ]; then
    alias i3c="$EDITOR ~/.config/i3/config"
else
    alias i3c="/usr/bin/nano ~/.config/i3/config"
fi

# pacman
alias pac="sudo pacman"
alias pacs="sudo pacman -S"
alias pacy="sudo pacman -Syy"
alias pacsyu="sudo pjacman -Syu"
alias pacss="sudo pacman -Ss"
alias pacr="sudo pacman -Rs"

