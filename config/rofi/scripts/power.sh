#!/bin/sh

basedir=$HOME/.config/rofi/scripts

shutdownicon=""
restarticon=""
suspendicon="鈴"
logouticon=""

options="$logouticon\n$suspendicon\n$restarticon\n$shutdownicon"
roficmd="rofi -width 781px -lines 4 -theme $basedir/power.rasi -dmenu"

selection="$(echo -e "$options" | $roficmd)"
case $selection in
  $logouticon)
    bspc quit
    ;;
  $suspendicon)
    systemctl suspend
    ;;
  $restarticon)
    systemctl reboot
    ;;
  $shutdownicon)
    systemctl poweroff
    ;;
esac
